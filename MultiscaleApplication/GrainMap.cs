﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiscaleApplication
{
    class GrainMap
    {
        public Grain[,] Map = new Grain[302, 302];

        public GrainMap()
        {
            for (int i = 0; i < 301; i++)
            {
                for (int j = 0; j < 301; j++)
                {
                    Map[i, j] = new Grain();
                }
            }
        }
    }    
}
