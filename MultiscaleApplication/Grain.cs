﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiscaleApplication
{
    class Grain
    {
        //int Xcor, Ycor;
        Color color;
        static List<Color> Colors;
        internal enum states {empty, grain, inclusion};
        internal states State;

        public Color Color { get => color; set => color = Color; }

        public Grain()
        {
            color = Color.White;
            Colors = new List<Color>();
            State = states.empty;
        }



        public void setGrColor(Color c)
        {
            color = c;
            State = states.grain;
        }


        public void setColor()
        {
            Random r = new Random();

            do
            {
                color = Color.FromArgb(r.Next(0, 256), r.Next(0, 256), r.Next(0, 256));
                State = states.grain;
                
            } while (Colors.Contains(color) || (color == Color.Black) );
            
            Colors.Add(color);
  
          
        }
    }
}
