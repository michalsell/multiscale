﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiscaleApplication
{
   internal class GrainMapEventArgs : EventArgs
    {
        public GrainMap gm;
        public int NumberOfGrains = 5;
        public int numinclusions, inclSize;
        public String inclType;
        

        public GrainMapEventArgs(GrainMap Grmap)
        {
            gm = Grmap;
        }

        public GrainMapEventArgs()
        {

        }
    }
}
