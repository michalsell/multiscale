﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiscaleApplication
{
    class Utils
    {
        internal static void copyMap(GrainMap pr , GrainMap prv)
        {
            for (int i = 1; i < 300 ; i++)
            {
                for (int j = 1; j < 300; j++)
                {
                    prv.Map[i, j].setGrColor(pr.Map[i, j].Color);
                    prv.Map[i, j].State = pr.Map[i, j].State;
                }
            }
        }

        internal static void msg(String s)
        {
            MessageBoxButtons mbb = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show(s, "", mbb);
        }
    }
}
