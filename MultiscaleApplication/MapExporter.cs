﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiscaleApplication
{
    class MapExporter
    {
       internal void exportTxt(String filename, GrainMap g)
        {

            StreamWriter Sw = new StreamWriter(filename + ".txt");


            for (int x = 1; x < 300; x++)
            {
                for (int y = 1; y < 300; y++)
                {
                    Sw.WriteLine(x + "," + y + "," + 
                        g.Map[x, y].Color.R + "," + 
                        g.Map[x, y].Color.G + "," + 
                        g.Map[x, y].Color.B + "," +
                        g.Map[x, y].State );
                }
            }
            Sw.Flush();
            MessageBoxButtons mbb = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show("File was saved!", "File save", mbb);
        }
    }
}
