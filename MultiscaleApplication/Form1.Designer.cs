﻿using System;

namespace MultiscaleApplication
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.drawButton = new System.Windows.Forms.Button();
            this.textBoxGrainCount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbRectangular = new System.Windows.Forms.RadioButton();
            this.rbRound = new System.Windows.Forms.RadioButton();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.tbNumber = new System.Windows.Forms.TextBox();
            this.tbSize = new System.Windows.Forms.TextBox();
            this.lbNumber = new System.Windows.Forms.Label();
            this.lbSize = new System.Windows.Forms.Label();
            this.gbInclusions = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnBoundaries = new System.Windows.Forms.Button();
            this.cbRemoveGr = new System.Windows.Forms.CheckBox();
            this.tbWidth = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbInclusions.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // drawButton
            // 
            this.drawButton.BackColor = System.Drawing.Color.MediumPurple;
            this.drawButton.FlatAppearance.BorderSize = 0;
            this.drawButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.drawButton.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.drawButton.Location = new System.Drawing.Point(110, 373);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(75, 23);
            this.drawButton.TabIndex = 1;
            this.drawButton.Text = "Draw";
            this.drawButton.UseVisualStyleBackColor = false;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // textBoxGrainCount
            // 
            this.textBoxGrainCount.Location = new System.Drawing.Point(346, 73);
            this.textBoxGrainCount.Name = "textBoxGrainCount";
            this.textBoxGrainCount.Size = new System.Drawing.Size(41, 20);
            this.textBoxGrainCount.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Location = new System.Drawing.Point(343, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Random Grain Count";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.MediumPurple;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(809, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveFileToolStripMenuItem,
            this.OpenFileMenuItem});
            this.toolStripMenuItem1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.toolStripMenuItem1.Image = global::MultiscaleApplication.Properties.Resources.icons8_folder_50;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(54, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // saveFileToolStripMenuItem
            // 
            this.saveFileToolStripMenuItem.Name = "saveFileToolStripMenuItem";
            this.saveFileToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.saveFileToolStripMenuItem.Text = "Save file";
            this.saveFileToolStripMenuItem.Click += new System.EventHandler(this.saveFileToolStripMenuItem_Click);
            // 
            // OpenFileMenuItem
            // 
            this.OpenFileMenuItem.BackColor = System.Drawing.Color.White;
            this.OpenFileMenuItem.Name = "OpenFileMenuItem";
            this.OpenFileMenuItem.Size = new System.Drawing.Size(124, 22);
            this.OpenFileMenuItem.Text = "Open file";
            this.OpenFileMenuItem.Click += new System.EventHandler(this.OpenFileMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.DarkGray;
            this.pictureBox1.Location = new System.Drawing.Point(12, 46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(300, 300);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // rbRectangular
            // 
            this.rbRectangular.AutoSize = true;
            this.rbRectangular.Location = new System.Drawing.Point(20, 25);
            this.rbRectangular.Name = "rbRectangular";
            this.rbRectangular.Size = new System.Drawing.Size(59, 17);
            this.rbRectangular.TabIndex = 5;
            this.rbRectangular.TabStop = true;
            this.rbRectangular.Tag = "0";
            this.rbRectangular.Text = "Square";
            this.rbRectangular.UseVisualStyleBackColor = true;
            // 
            // rbRound
            // 
            this.rbRound.AutoSize = true;
            this.rbRound.Location = new System.Drawing.Point(20, 48);
            this.rbRound.Name = "rbRound";
            this.rbRound.Size = new System.Drawing.Size(57, 17);
            this.rbRound.TabIndex = 6;
            this.rbRound.TabStop = true;
            this.rbRound.Tag = "1";
            this.rbRound.Text = "Round";
            this.rbRound.UseVisualStyleBackColor = true;
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Checked = true;
            this.rbNone.Location = new System.Drawing.Point(20, 71);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 7;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            // 
            // tbNumber
            // 
            this.tbNumber.Location = new System.Drawing.Point(140, 44);
            this.tbNumber.Name = "tbNumber";
            this.tbNumber.Size = new System.Drawing.Size(39, 20);
            this.tbNumber.TabIndex = 8;
            this.tbNumber.Text = "1";
            // 
            // tbSize
            // 
            this.tbSize.Location = new System.Drawing.Point(140, 97);
            this.tbSize.Name = "tbSize";
            this.tbSize.Size = new System.Drawing.Size(39, 20);
            this.tbSize.TabIndex = 9;
            this.tbSize.Text = "1";
            // 
            // lbNumber
            // 
            this.lbNumber.AutoSize = true;
            this.lbNumber.Location = new System.Drawing.Point(134, 25);
            this.lbNumber.Name = "lbNumber";
            this.lbNumber.Size = new System.Drawing.Size(44, 13);
            this.lbNumber.TabIndex = 10;
            this.lbNumber.Text = "Number";
            // 
            // lbSize
            // 
            this.lbSize.AutoSize = true;
            this.lbSize.Location = new System.Drawing.Point(134, 78);
            this.lbSize.Name = "lbSize";
            this.lbSize.Size = new System.Drawing.Size(65, 13);
            this.lbSize.TabIndex = 11;
            this.lbSize.Text = "Size/Radius";
            // 
            // gbInclusions
            // 
            this.gbInclusions.BackColor = System.Drawing.Color.Transparent;
            this.gbInclusions.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gbInclusions.Controls.Add(this.btnAdd);
            this.gbInclusions.Controls.Add(this.rbRectangular);
            this.gbInclusions.Controls.Add(this.lbSize);
            this.gbInclusions.Controls.Add(this.rbNone);
            this.gbInclusions.Controls.Add(this.tbSize);
            this.gbInclusions.Controls.Add(this.lbNumber);
            this.gbInclusions.Controls.Add(this.rbRound);
            this.gbInclusions.Controls.Add(this.tbNumber);
            this.gbInclusions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.gbInclusions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbInclusions.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.gbInclusions.Location = new System.Drawing.Point(565, 46);
            this.gbInclusions.Margin = new System.Windows.Forms.Padding(0);
            this.gbInclusions.Name = "gbInclusions";
            this.gbInclusions.Padding = new System.Windows.Forms.Padding(0);
            this.gbInclusions.Size = new System.Drawing.Size(208, 178);
            this.gbInclusions.TabIndex = 12;
            this.gbInclusions.TabStop = false;
            this.gbInclusions.Text = "INCLUSIONS";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Lavender;
            this.btnAdd.Enabled = false;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Location = new System.Drawing.Point(20, 132);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(57, 23);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnBoundaries
            // 
            this.btnBoundaries.BackColor = System.Drawing.Color.Lavender;
            this.btnBoundaries.Enabled = false;
            this.btnBoundaries.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBoundaries.Location = new System.Drawing.Point(7, 19);
            this.btnBoundaries.Name = "btnBoundaries";
            this.btnBoundaries.Size = new System.Drawing.Size(72, 23);
            this.btnBoundaries.TabIndex = 13;
            this.btnBoundaries.Text = "Boundaries";
            this.btnBoundaries.UseVisualStyleBackColor = false;
            this.btnBoundaries.Click += new System.EventHandler(this.btnBoundaries_Click);
            // 
            // cbRemoveGr
            // 
            this.cbRemoveGr.AutoSize = true;
            this.cbRemoveGr.BackColor = System.Drawing.Color.Gray;
            this.cbRemoveGr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbRemoveGr.Location = new System.Drawing.Point(98, 22);
            this.cbRemoveGr.Name = "cbRemoveGr";
            this.cbRemoveGr.Size = new System.Drawing.Size(96, 17);
            this.cbRemoveGr.TabIndex = 14;
            this.cbRemoveGr.Text = "Remove Grains";
            this.cbRemoveGr.UseVisualStyleBackColor = false;
            // 
            // tbWidth
            // 
            this.tbWidth.Location = new System.Drawing.Point(95, 76);
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.Size = new System.Drawing.Size(35, 20);
            this.tbWidth.TabIndex = 15;
            this.tbWidth.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Boundary Width";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnBoundaries);
            this.groupBox1.Controls.Add(this.tbWidth);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cbRemoveGr);
            this.groupBox1.Location = new System.Drawing.Point(565, 261);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(208, 120);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "BOUNDARIES";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(809, 496);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbInclusions);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxGrainCount);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "GrainGrowthSimulation";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbInclusions.ResumeLayout(false);
            this.gbInclusions.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Button drawButton;
        public System.Windows.Forms.TextBox textBoxGrainCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem OpenFileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveFileToolStripMenuItem;
        private System.Windows.Forms.RadioButton rbRectangular;
        private System.Windows.Forms.RadioButton rbRound;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.TextBox tbNumber;
        private System.Windows.Forms.TextBox tbSize;
        private System.Windows.Forms.Label lbNumber;
        private System.Windows.Forms.Label lbSize;
        private System.Windows.Forms.GroupBox gbInclusions;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnBoundaries;
        private System.Windows.Forms.CheckBox cbRemoveGr;
        private System.Windows.Forms.TextBox tbWidth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;

        public delegate void drawButtonClickedEventHandler(object source, EventArgs args);
        public event drawButtonClickedEventHandler drawButtonClicked;

        //internal event EventHandler<GrainMapEventArgs> drawButtonClicked;
        

        

    }
}

