﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiscaleApplication
{
    class GrainCalculator
    {
        GrainMap PresentMap;
        GrainMap PreviousMap;
        List<Color> NeighColors;
        public delegate  void calculationEndedHandler(object src, GrainMapEventArgs e);
        public event calculationEndedHandler CalculationEnded;  

        
        public GrainCalculator()
        {

        }
        
        public GrainCalculator(GrainMap GrainMap)
        {
            PresentMap = GrainMap;
            PreviousMap = new GrainMap();
            NeighColors = new List<Color>();

            for (int xx = 1; xx < 300; xx++)
            {
                for (int yy = 1; yy < 300; yy++)
                {
                    PreviousMap.Map[xx, yy].setGrColor(PresentMap.Map[xx, yy].Color); 
                }

            }

        }
        
        public void onRandomFinished(object src, GrainMapEventArgs e)
        {
            PresentMap = e.gm;
            PreviousMap = new GrainMap();
            NeighColors = new List<Color>();

            Utils.copyMap(PresentMap, PreviousMap);

            //for (int xx = 1; xx < 301; xx++)
            //{
            //    for (int yy = 1; yy < 301; yy++)
            //    {
            //        PreviousMap.Map[xx, yy].setGrColor(PresentMap.Map[xx, yy].Color);
            //    }

            //}

            Console.WriteLine("begining calculation");
            calculate();
            Console.WriteLine("Calc has ended!");

            if (CalculationEnded != null)
            {
                e.gm = this.PresentMap;
                Console.WriteLine("calc ended event fired!");
               
                CalculationEnded.Invoke(this, e );
                
            }
        }

        public void setPresentMap(GrainMap gm)
        {
            PresentMap = gm;
            Console.WriteLine("PresentMap set.");
        }

        public GrainMap retPreviousMap()
        {
            return PreviousMap;
        }

        public GrainMap retPresentMap()
        {
            return PresentMap;
        }

        public GrainMap calculate()
        {
           
            bool passChanged = false;

            do
            {
                passChanged = false;    

                for (int x = 1; x < 300; x++)
                {
                    for (int y = 1; y < 300; y++)
                    {
                        //grainChanged = false;

                        if ( PreviousMap.Map[x, y].State == Grain.states.empty  )
                             
                        {
                            

                            if (PreviousMap.Map[x - 1, y].State == Grain.states.grain) NeighColors.Add(PreviousMap.Map[x - 1, y].Color);
                            if (PreviousMap.Map[x + 1, y].State == Grain.states.grain) NeighColors.Add(PreviousMap.Map[x + 1, y].Color);
                            if (PreviousMap.Map[x, y - 1].State == Grain.states.grain) NeighColors.Add(PreviousMap.Map[x, y - 1].Color);
                            if (PreviousMap.Map[x, y + 1].State == Grain.states.grain) NeighColors.Add(PreviousMap.Map[x, y + 1].Color);

                            

                            if (NeighColors.Count != 0 || NeighColors.Count > 3)
                            {
                                var mostFrequentColor = NeighColors.GroupBy(c => c)
                                .Select(c => new { col = c, count = c.Count() })
                                .OrderByDescending(h => h.count)
                                .Select(h => h.col).First().ToArray();


                                PresentMap.Map[x, y].setGrColor(mostFrequentColor[0]);
                                PresentMap.Map[x, y].State = Grain.states.grain;
                                passChanged = true;
                            }
                            NeighColors.Clear();

                        }
                    }
                }



                //copy
                Utils.copyMap(PresentMap, PreviousMap);


                //for (int xx = 1; xx < 300; xx++)
                //{
                //    for (int yy = 1; yy < 300; yy++)
                //    {
                //        PreviousMap.Map[xx, yy].setGrColor(PresentMap.Map[xx, yy].Color);
                //    }

                //}

            } while (passChanged == true);



            //if (CalculationEnded != null)
            //{
            //    CalculationEnded.Invoke(this, new GrainMapEventArgs(PresentMap));
            //}

            return PresentMap;
        }

        public GrainMap selectBoundaryGrains(GrainMap g, int width, Boolean remove)
        {
            GrainMap gg = g;

            


                for (int x = 1; x < 300; x++)
                {
                    for (int y = 1; y < 300; y++)
                    {

                        if (PreviousMap.Map[x - 1, y].State == Grain.states.grain) NeighColors.Add(PreviousMap.Map[x - 1, y].Color);
                        if (PreviousMap.Map[x + 1, y].State == Grain.states.grain) NeighColors.Add(PreviousMap.Map[x + 1, y].Color);
                        if (PreviousMap.Map[x, y - 1].State == Grain.states.grain) NeighColors.Add(PreviousMap.Map[x, y - 1].Color);
                        if (PreviousMap.Map[x, y + 1].State == Grain.states.grain) NeighColors.Add(PreviousMap.Map[x, y + 1].Color);


                        var allAreSame = NeighColors.All(c => c == NeighColors.First());
                        NeighColors.Clear();


                        if (!allAreSame)
                        {
                            for (int q = 0; q < width; q++)
                            {
                                if (((x + q) <= 300) && ((y + q) <= 300))
                                {
                                    PresentMap.Map[x + q, y].setGrColor(Color.Black);
                                    PresentMap.Map[x + q, y].State = Grain.states.inclusion;

                                    PresentMap.Map[x, y + q].setGrColor(Color.Black);
                                    PresentMap.Map[x, y + q].State = Grain.states.inclusion;
                            }
                            }
                            
                        }

                    }

                }
                Utils.copyMap(PresentMap, PreviousMap);
            

            if (remove) removeGrains(gg);

            return gg;
        }




        public GrainMap removeGrains(GrainMap g)
        {
            GrainMap gg = g;


            for (int x = 1; x < 300; x++)
            {
                for (int y = 1; y < 300; y++)
                {


                    if (gg.Map[x,y].Color != Color.Black)
                    {
                        gg.Map[x, y].setGrColor(Color.White);
                        gg.Map[x, y].State = Grain.states.empty;
                    }

                }

            }

            return gg;
        }
     
    }
}
