﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiscaleApplication
{

    //to draw a grain layout on a picturebox
    internal class Drawer
    {

        PictureBox pb;

        public Drawer(PictureBox pictureBox)
        {
            pb = pictureBox;
        }


        public void onDrawButtonClicked(object src, EventArgs e)
        {
            drawRedTestElipse();
        }

        public void onCalculationEnded(object src, GrainMapEventArgs e)
        {
            
            draw(e.gm);
            Console.WriteLine("Map Drawn!");
        }

        public void drawRedTestElipse()
        {
            Bitmap DrawArea;
            DrawArea = new Bitmap(300, 300);

            Graphics g;
            g = Graphics.FromImage(DrawArea);


            Pen RedPen = new Pen(Color.Red);

            g.DrawEllipse(RedPen, 10, 20, 30, 40);

            pb.Image = DrawArea;

        }

        public void clearDrawingSpace()
        {
            Bitmap DrawArea;
            DrawArea = new Bitmap(300, 300);

            Graphics g;
            g = Graphics.FromImage(DrawArea);

            g.Clear(Color.White);
            pb.Image = DrawArea;
        }


        public void draw(GrainMap map)
        {
            Bitmap DrawArea;
            DrawArea = new Bitmap(300, 300);

            Graphics g;
            g = Graphics.FromImage(DrawArea);

            for (int i = 1; i < 300; i++)
            {
                for (int j = 1; j < 300; j++)
                {
                    Pen GrainPen = new Pen( map.Map[i,j].Color);
                       
                    g.DrawRectangle(GrainPen,i-1, j-1, 1, 1);

                }
            }
            
            pb.Image = DrawArea;
        }
    }
}
