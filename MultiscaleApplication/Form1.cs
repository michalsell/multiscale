﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiscaleApplication
{
    public partial class Form1 : Form
    {

        internal delegate void userInputSentHandler(object src, GrainMapEventArgs e);
        internal event userInputSentHandler userInputSent;
        internal GrainCalculator GrainCalc;

        public PictureBox PictureBox1 {
            get { return pictureBox1;  } 
        }

        internal Form1(GrainCalculator GC)
        {
            InitializeComponent();
            GrainCalc = GC;
        }

        

        //event publisher 
        internal void drawButton_Click(object sender, EventArgs e)
        {
            //if (drawButtonClicked != null)
            //{
            //    drawButtonClicked?.Invoke(this, new EventArgs());
            //}


            if(userInputSent != null)
            {
                Console.WriteLine("Button Clicked!");
                GrainMapEventArgs gmArgs = new GrainMapEventArgs();
                gmArgs.NumberOfGrains = Int32.Parse(textBoxGrainCount.Text);
                gmArgs.inclSize = Int32.Parse(tbSize.Text);
                gmArgs.numinclusions = Int32.Parse(tbNumber.Text);

                //which radiobtn is checked
                var checkedRadioBtn = gbInclusions.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
                gmArgs.inclType = checkedRadioBtn.Text;

                userInputSent?.Invoke(this, gmArgs);
            }

            btnBoundaries.Enabled = true;
            btnBoundaries.BackColor = Color.MediumPurple;
            btnAdd.Enabled = true;
            btnAdd.BackColor = Color.MediumPurple;

        }

        private void OpenFileMenuItem_Click(object sender, EventArgs e)
        {
            GrainMap gm;
            OpenFileDialog Ofd = new OpenFileDialog();            
            Ofd.Filter = "BMP (*.bmp)|*.bmp| TXT (*.txt)|*.txt";
            if(Ofd.ShowDialog() == DialogResult.OK)
            {
                var extension = Path.GetExtension(Ofd.FileName);
                Drawer dr = new Drawer(pictureBox1);
                MapImporter Mi = new MapImporter();

                switch (extension.ToLower())
                {
                    case ".bmp":
                        gm = Mi.importBmp(Ofd.FileName);
                        dr.draw(gm);
                        GrainCalc.setPresentMap(gm);
                        break;
                    case ".txt":
                        gm = Mi.importTxt(Ofd.FileName);
                        dr.draw(gm);
                        GrainCalc.setPresentMap(gm);
                        break;
                }
            }
            
            
            

        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog Sfd = new SaveFileDialog();
            Sfd.Filter = "BMP (*.bmp)|*.bmp| TXT(*.txt)|*.txt";
            Bitmap bmp = new Bitmap(pictureBox1.Image);
            if (Sfd.ShowDialog() == DialogResult.OK)
            {
                var extension =  Path.GetExtension(Sfd.FileName);

                switch (extension.ToLower())
                {
                    case ".bmp":
                        bmp.Save(Sfd.FileName);
                        MessageBoxButtons mbb = MessageBoxButtons.OK;
                        DialogResult result;
                        result = MessageBox.Show("File was saved!", "File save", mbb);
                        break;
                    case ".txt":
                        
                        MapExporter Me = new MapExporter();
                        Me.exportTxt(Sfd.FileName, GrainCalc.retPresentMap());
                        break;
                }

                
            }
            
            

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var checkedRadioBtn = gbInclusions.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
            String inclTypeStr = checkedRadioBtn.Text;

            if (inclTypeStr != "None")
            {
                GRrandomizer gr = new GRrandomizer();
                GRrandomizer.InclusionTypes it = GRrandomizer.InclusionTypes.round;



                switch (inclTypeStr)
                {
                    case "Square":
                        it = GRrandomizer.InclusionTypes.square;
                        break;
                    case "Round":
                        it = GRrandomizer.InclusionTypes.round;
                        break;
                    case "None":
                        it = GRrandomizer.InclusionTypes.none;
                        break;
                }

                GrainMap G = gr.PostRandomizeInclusions(GrainCalc.retPresentMap(), Int32.Parse(tbNumber.Text), it, Int32.Parse(tbSize.Text));
                GrainCalc.setPresentMap(G);
                Drawer dr = new Drawer(pictureBox1);
                dr.draw(GrainCalc.retPresentMap());

            }
        }

        private void btnBoundaries_Click(object sender, EventArgs e)
        {
            var remove = cbRemoveGr.Checked;
            int width = Int32.Parse(tbWidth.Text);
            GrainMap g = GrainCalc.selectBoundaryGrains(GrainCalc.retPresentMap(), width, remove);
            Drawer d = new Drawer(pictureBox1);
            //GrainCalc.setPresentMap(g);
            d.draw(g);
        }

        
    }
}
