﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MultiscaleApplication
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            GrainCalculator calculator = new GrainCalculator();

            Form1 MainWindow = new Form1(calculator);

           

            Drawer MyDrawer = new Drawer(MainWindow.PictureBox1);
           
            
            GRrandomizer grr = new GRrandomizer();



            MainWindow.userInputSent += grr.onUserInputSent;
            grr.RndCompleted += calculator.onRandomFinished;
            calculator.CalculationEnded += MyDrawer.onCalculationEnded;
            
                 
           
            Application.Run(MainWindow);

            
        }
    }
}
