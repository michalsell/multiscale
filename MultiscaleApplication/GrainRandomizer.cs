﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiscaleApplication
{
    internal class GrainRandomizer 
    {
        public GrainRandomizer()
        {
           
        }

        public void onUserInputSent(object src, GrainMapEventArgs e)
        {
            GrainMap gm = new GrainMap();
            
        }

        public void Randomize(GrainMap Gm, int number)
        {

            Random r = new Random();


            for (int i = 0; i < number; i++)
            {
                int x = r.Next(1, 301);
                int y = r.Next(1, 301);

                Gm.Map[x, y].setColor();
            }
        }

        public void onDrawButtonClicked(object src, GrainMapEventArgs Gm)
        {
            Randomize(Gm.gm, Gm.number);
        }
    }
}
