﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiscaleApplication
{
    class GRrandomizer
    {
        internal delegate void RndCompletedEventHandler(object src, GrainMapEventArgs gm);
        internal event RndCompletedEventHandler RndCompleted;
        public enum InclusionTypes {square, round, none};
        InclusionTypes InclusionType ; 


        public void onUserInputSent(object src, GrainMapEventArgs ge)
        {
            GrainMap gm = new GrainMap();


            switch (ge.inclType)
            {
                case "Square":
                    InclusionType = InclusionTypes.square;
                    break;
                case "Round":
                    InclusionType = InclusionTypes.round;
                    break;
                case "None":
                    InclusionType = InclusionTypes.none;
                    break;
            }
            

            ge.gm = RandomizeInclusions(gm, ge.numinclusions, InclusionType, ge.inclSize);
            ge.gm = Randomize(ge.gm, ge.NumberOfGrains);
            Console.WriteLine("Grain map created with " + ge.NumberOfGrains + " grains");


            if (RndCompleted != null)
            {
                RndCompleted.Invoke(this, ge);
            }

        }


        public GrainMap Randomize(GrainMap Gm, int number)
        {

            Random r = new Random();


            for (int i = 0; i < number; i++)
            {
                int x = r.Next(1, 301);
                int y = r.Next(1, 301);

                if(Gm.Map[x,y].State != Grain.states.empty)
                {
                    i--;
                }
                else
                {
                    Gm.Map[x, y].setColor();
                    Gm.Map[x, y].State = Grain.states.grain;
                }

                
            }
            return Gm;
        }

        public GrainMap RandomizeInclusions(GrainMap Gm, int number, InclusionTypes type, int size)
        {
            Random r = new Random();

            for (int i = 0; i < number; i++)
            {
                int x = r.Next(1, 301-size);
                int y = r.Next(1, 301-size);

                if (Gm.Map[x, y].State != Grain.states.empty)
                {
                    i--;
                }
                else
                {
                    if (type == InclusionTypes.square)
                    {
                        for (int ii = x; ii < x + size - 1; ii++)
                        {
                            for (int jj = y; jj < y + size - 1; jj++)
                            {
                                if ((ii <= 300) && (jj <= 300))
                                {
                                    Gm.Map[ii, jj].setGrColor(Color.Black);
                                    Gm.Map[ii, jj].State = Grain.states.inclusion;
                                }
                            }
                        }
                    }
                    if (type == InclusionTypes.round)
                    {
                        for (int ii = 1; ii < 301; ii++)
                        {
                            for (int jj = 1; jj < 301; jj++)
                            {
                                if ((ii <= 300) && (jj <= 300))
                                {
                                    //rownanie okregu
                                    if ( (ii-x) * (ii-x) + (jj-y) * (jj-y) <= size*2) {
                                        Gm.Map[ii, jj].setGrColor(Color.Black);
                                        Gm.Map[ii, jj].State = Grain.states.inclusion;
                                    }
                                }
                            }
                        }
                    }
                    
                }
            }
            
            Console.WriteLine("Added " + number + " inclusions " + "of type " + type + " ,size: " + size);
            return Gm;
        }




        public GrainMap PostRandomizeInclusions(GrainMap Gm, int number, InclusionTypes type, int size)
        {
            Random r = new Random();
            List<Color> NeighColors = new List<Color>();
            int missed = 0;

            for (int i = 0; i < number; i++)
            {
                int x = r.Next(1, 301 - size);
                int y = r.Next(1, 301 - size);


                if (Gm.Map[x - 1, y].State == Grain.states.grain) NeighColors.Add(Gm.Map[x - 1, y].Color);
                if (Gm.Map[x + 1, y].State == Grain.states.grain) NeighColors.Add(Gm.Map[x + 1, y].Color);
                if (Gm.Map[x, y - 1].State == Grain.states.grain) NeighColors.Add(Gm.Map[x, y - 1].Color);
                if (Gm.Map[x, y + 1].State == Grain.states.grain) NeighColors.Add(Gm.Map[x, y + 1].Color);

                var allAreSame = NeighColors.All(c => c == NeighColors.First());
                NeighColors.Clear();

                if (allAreSame)
                {                
                    i--;
                    missed++;
                }
                else
                {
                    if (type == InclusionTypes.square)
                    {
                        for (int ii = x; ii < x + size - 1; ii++)
                        {
                            for (int jj = y; jj < y + size - 1; jj++)
                            {
                                if ((ii <= 300) && (jj <= 300))
                                {
                                    Gm.Map[ii, jj].setGrColor(Color.Black);
                                    Gm.Map[ii, jj].State = Grain.states.inclusion;
                                }
                            }
                        }
                    }
                    if (type == InclusionTypes.round)
                    {
                        for (int ii = 1; ii < 301; ii++)
                        {
                            for (int jj = 1; jj < 301; jj++)
                            {
                                if ((ii <= 300) && (jj <= 300))
                                {
                                    //rownanie okregu
                                    if ((ii - x) * (ii - x) + (jj - y) * (jj - y) <= size * 2)
                                    {
                                        Gm.Map[ii, jj].setGrColor(Color.Black);
                                        Gm.Map[ii, jj].State = Grain.states.inclusion;
                                    }
                                }
                            }
                        }
                    }

                }
            }

            Console.WriteLine("Added " + number + " inclusions " + "of type " + type + " ,size: " + size);
            Console.WriteLine("missed: " + missed + "times.");
            return Gm;
        }
    }
}
