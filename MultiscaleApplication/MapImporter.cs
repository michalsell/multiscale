﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace MultiscaleApplication
{
    class MapImporter
    {
        internal GrainMap importTxt(String filename)
        {
            GrainMap gm = new GrainMap();
            var reader = new StreamReader(filename);
            List<int> intsRead = new List<int>();
            int val;

            while (!reader.EndOfStream) {
                var line = reader.ReadLine();
                var values = line.Split(',');

                for (int i = 0; i < values.Length-1; i++)
                {

                    Int32.TryParse(values[i], out val);
                    intsRead.Add(val);
                }
              
                gm.Map[ intsRead[0], intsRead[1] ].
                    setGrColor( Color.FromArgb(intsRead[2], intsRead[3], intsRead[4]) );

                intsRead.Clear();
            }

            return gm;
        }

        internal GrainMap importBmp(String filename)
        {
            GrainMap gm = new GrainMap();
            Bitmap bmp = new Bitmap(filename);

            for (int i = 0; i < bmp.Width; i++)
            {
                for (int j = 0; j < bmp.Height; j++)
                {
                    var colorHash = bmp.GetPixel(i, j).GetHashCode();
                    Color c = Color.FromArgb(colorHash);
                    gm.Map[i, j].setGrColor(c);
                }
            }
            
            return gm;
        }
    }
}
