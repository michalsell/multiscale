﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    class Calculator
    {

        private static Space previousSpace;
        private static List<Color> NeighColors;

        public Calculator()
        {
            previousSpace = Factory.getSpace();
            NeighColors = Factory.getList<Color>();

        }

        public Space calculateMC(Space s, int stepsNum)
        {
            int debugGrainCount = 0;
            List <Grain> unlockedGrainsFromSpace = Factory.getList<Grain>();
            List<Grain> MooreNeighborGrains = Factory.getList<Grain>();
            List<Color> MooreNeighborGrainColors = Factory.getList<Color>();
            
            for (int i = 0; i < stepsNum; i++)
            {

                //copy grains from Space to list 
                for (int x = 1; x < s.loopSize; x++)
                {
                    for (int y = 1; y < s.loopSize; y++)
                    {
                        if (s[x, y].State == States.grain && s[x, y].isLocked == false)
                        {
                           unlockedGrainsFromSpace.Add(s[x, y]);
                        }
                    }
                }

                //shuffle grains
               unlockedGrainsFromSpace.Shuffle();


                foreach (var Grain in unlockedGrainsFromSpace)
                {
                    
                        Color col;
                    //get all neigbors on a list
                    // all are unlocked so > if < doesnt apply

                    col = s[Grain.x + 1, Grain.y].Color;
                    if(col != Color.White)
                        if(s[Grain.x + 1, Grain.y].isLocked == false)
                        MooreNeighborGrainColors.Add(col);

                    col = s[Grain.x - 1, Grain.y+1].Color;
                    if (col != Color.White)
                        if (s[Grain.x - 1, Grain.y + 1].isLocked == false)
                            MooreNeighborGrainColors.Add(col);

                    col = s[Grain.x-1, Grain.y].Color;
                    if (col != Color.White)
                        if (s[Grain.x - 1, Grain.y].isLocked == false)
                            MooreNeighborGrainColors.Add(col);

                    col = s[Grain.x -1 , Grain.y - 1].Color;
                    if (col != Color.White)
                        if (s[Grain.x - 1, Grain.y - 1].isLocked == false)
                            MooreNeighborGrainColors.Add(col);

                    col = s[Grain.x, Grain.y - 1].Color;
                    if (col != Color.White)
                        if (s[Grain.x, Grain.y - 1].isLocked == false)
                            MooreNeighborGrainColors.Add(col);

                    col = s[Grain.x + 1, Grain.y - 1].Color;
                    if (col != Color.White)
                        if (s[Grain.x + 1, Grain.y - 1].isLocked == false)
                            MooreNeighborGrainColors.Add(col);

                    col = s[Grain.x + 1, Grain.y].Color;
                    if (col != Color.White)
                        if (s[Grain.x + 1, Grain.y].isLocked == false)
                            MooreNeighborGrainColors.Add(col);

                    col = s[Grain.x + 1, Grain.y + 1].Color;
                    if (col != Color.White)
                        if (s[Grain.x + 1, Grain.y + 1].isLocked == false)
                            MooreNeighborGrainColors.Add(col);



                    //s[Grain.x - 1, Grain.y].Color == false)
                    //        MooreNeighborGrains.Add(Grain);
                    //    if (s[Grain.x, Grain.y + 1].Color == false)
                    //        MooreNeighborGrains.Add(Grain);
                    //    if (s[Grain.x, Grain.y - 1].Color == false)
                    //        MooreNeighborGrains.Add(Grain);
                    //    if (s[Grain.x + 1, Grain.y + 1].Color == false)
                    //        MooreNeighborGrains.Add(Grain);
                    //    if (s[Grain.x - 1, Grain.y + 1].Color == false)
                    //        MooreNeighborGrains.Add(Grain);
                    //    if (s[Grain.x - 1, Grain.y - 1].Color == false)
                    //        MooreNeighborGrains.Add(Grain);
                    //    if (s[Grain.x + 1, Grain.y - 1].Color == false)
                    //        MooreNeighborGrains.Add(Grain);

                   
                        Color ColorMinimizingEnergy =
                        EnergyCalc.
                        getRandomColorMinimizingEnergy(s[Grain.x, Grain.y].Color, MooreNeighborGrainColors);

                        s[Grain.x, Grain.y].Color = ColorMinimizingEnergy;


                        ////select the most frequent
                        //var mostFrequentColor = MooreNeighborGrainColors.GroupBy(c => c)
                        //            .Select(c => new { col = c, count = c.Count() })
                        //            .OrderByDescending(h => h.count)
                        //            .Select(h => h.col).First().ToArray();
                        ////swap current for the most frequent color
                        //s[Grain.x, Grain.y].Color = mostFrequentColor[0];
                        MooreNeighborGrainColors.Clear();

                    //++debugGrainCount;
                    //Utils.msg("grain num: " + debugGrainCount + "\n");
                }
               unlockedGrainsFromSpace.Clear();
            }
            SpaceLocker.LockSpace(s);
            SpaceLocker.LockSpace(previousSpace);
            Utils.MarkBoundaryGrains(s);
            return s;
        }


        public Space calculateCA(Space s)
        {
            Utils.msg("Begining calculationCA...");
            Utils.copySpace(s, previousSpace);
            bool passChanged = false;

            do
            {
                passChanged = false;

                for (int x = 1; x < 300; x++)
                {
                    for (int y = 1; y < 300; y++)
                    {
                        if (previousSpace[x, y].State == States.empty && previousSpace[x, y].isLocked == false)

                        {


                            if (previousSpace[x - 1, y].State == States.grain 
                                && previousSpace[x - 1, y].isLocked == false) NeighColors.Add(previousSpace[x - 1, y].Color);

                            if (previousSpace[x + 1, y].State == States.grain
                                && previousSpace[x + 1, y].isLocked == false) NeighColors.Add(previousSpace[x + 1, y].Color);

                            if (previousSpace[x, y - 1].State == States.grain
                                && previousSpace[x, y - 1].isLocked == false) NeighColors.Add(previousSpace[x, y - 1].Color);

                            if (previousSpace[x, y + 1].State == States.grain
                                && previousSpace[x, y + 1].isLocked == false) NeighColors.Add(previousSpace[x, y + 1].Color);



                            if (NeighColors.Count != 0 || NeighColors.Count > 3)
                            {
                                var mostFrequentColor = NeighColors.GroupBy(c => c)
                                .Select(c => new { col = c, count = c.Count() })
                                .OrderByDescending(h => h.count)
                                .Select(h => h.col).First().ToArray();


                                s[x, y].Color = mostFrequentColor[0];
                                s[x, y].State = States.grain;
                                passChanged = true;
                            }
                            NeighColors.Clear();

                        }
                    }
                }

                //copy
                Utils.copySpace(s, previousSpace);

            } while (passChanged == true);
            SpaceLocker.LockSpace(s);
            SpaceLocker.LockSpace(previousSpace);
            Utils.MarkBoundaryGrains(s);
            return s;
        }

    }
}
