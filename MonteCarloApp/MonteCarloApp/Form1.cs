﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonteCarloApp
{
    public partial class Form1 : Form
    {
        Space space;
        Space energySpace;
        Drawer drawer;
        Calculator calculator;
        Color lastClickedColor;


        public Form1()
        {
            InitializeComponent();
            drawer = Factory.getDrawer(PictureBox1);
            calculator = Factory.getCalculator();
            energySpace = Factory.getSpace();
        }

        public PictureBox PictureBox1
        {
            get { return pictureBox1; }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            tbClickedColor.BackColor = Color.White;
            btnRemoveGrains.Enabled = false;
            cbFillEmpty.Enabled = true;
            cbSwitchView.Enabled = true;


            if (!cbFillEmpty.Checked)
            {
                space = Factory.getSpace();
            }

            if (rbCA.Checked)
            {
                GrainRandomizer.randomizeCA(space, Utils.getIntFromTextBox(tbGrainCount));
                
                calculator.calculateCA(space);
                drawer.drawSpace(space);
            }
            else
            {
                GrainRandomizer.randomizeMC(space, Utils.getIntFromTextBox(tbGrainCount));
             
                calculator.calculateMC(space, Utils.getIntFromTextBox(tbStepsCount));
                drawer.drawSpace(space);
            }

            
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            MouseEventArgs me = (MouseEventArgs)e;


            for (int x = 1; x < 301; x++)
            {
                for (int y = 1; y < 301; y++)
                {
                    if (space[x, y].Color == Color.Cyan)
                        space[x, y].Color = lastClickedColor;
                }
            }
            drawer.drawSpace(space);

            
            Bitmap bitmap = new Bitmap(pictureBox1.Image);
            Color c = bitmap.GetPixel(me.Location.X, me.Location.Y);

            if (c != Color.Cyan)
            {
                tbClickedColor.BackColor = c;

                for (int x = 1; x < 301; x++)
                {
                    for (int y = 1; y < 301; y++)
                    {
                        if (space[x, y].Color == c)
                            space[x, y].Color = Color.Cyan;
                    }
                }
                lastClickedColor = c;
            }
            drawer.drawSpace(space);
            btnRemoveGrains.Enabled = true;
        }

        private void btnRemoveGrains_MouseClick(object sender, MouseEventArgs e)
        {
            space = Utils.removeColorAndSetToEmpty(space);
            drawer.drawSpace(space);
            tbClickedColor.BackColor = Color.White;
        }

        private void cbSwitchView_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSwitchView.Checked)
            {
                Utils.copySpace(space, energySpace);
                int ge = Utils.getIntFromTextBox(tbGrainEnergy);
                if (rbHeterogernous.Checked == true)
                {
                    int be = Utils.getIntFromTextBox(tbBoundaryEnergy);
                    Utils.ColorEnergyView(energySpace, ge, be);
                }
                else
                {
                    Utils.ColorEnergyView(energySpace, ge);
                }

                drawer.drawSpace(energySpace);
            }
            else
                drawer.drawSpace(space);
        }

        private void rbHeterogenous_CheckedChanged(object sender, EventArgs e)
        {
            if (rbHeterogernous.Checked)
                tbBoundaryEnergy.Enabled = true;
            else
                tbBoundaryEnergy.Enabled = false;
        }

        private void btnRecrystalize_Click(object sender, EventArgs e)
        {
            NucleationType nucleationType;
            Locations location;
            var grainNum = Utils.getIntFromTextBox(tbNuclConstant);
            double J = Utils.getDoubleFromTextBox(tbJcoef);
            int numSteps = Utils.getIntFromTextBox(tbStepsCount);
            int GrainEnergy = Utils.getIntFromTextBox(tbGrainEnergy);
            int BoundaryEnergy = 1;

            if (rbHeterogernous.Checked)
                BoundaryEnergy = Utils.getIntFromTextBox(tbBoundaryEnergy);
            
           
            if (rbNuclBegining.Checked)
                nucleationType = NucleationType.BEGINING;
            if (rbNuclConstant.Checked)
                nucleationType = NucleationType.CONSTANT;
            else
                nucleationType = NucleationType.INCREASING;



            if (rbLocationAnywhere.Checked)
                location = Locations.ANYWHERE;
            else
                location = Locations.BOUNDARIES;


            if (rbHomogenous.Checked)
                BoundaryEnergy = GrainEnergy;

            Utils.assignEnergy(space, GrainEnergy, BoundaryEnergy);
            SRXCalculator.calculate(space, numSteps, grainNum, J, nucleationType, location);
            drawer.drawSpace(space);

            
        }
    }
}
