﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    class Space
    {
        public Grain[,] space = Factory.getGrainArray(302);
        public static List<Color> Colors;
        public List<Grain> BoundaryGrains;
        public int loopSize = 301;

        public Grain this[int x, int y]
        {
            get { return space[x, y]; }
        }

        public Space()
        {
            Colors = Factory.getList<Color>();
            BoundaryGrains = Factory.getList<Grain>();

            for (int i = 0; i < 302; i++)
            {
                for (int j = 0; j < 302; j++)
                {
                    space[i, j] = Factory.getGrain();
                    space[i, j].x = i;
                    space[i, j].y = j;
                }
            }
        }

    }
}
