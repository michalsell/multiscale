﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonteCarloApp
{
    class Utils
    {

        public static void copySpace(Space present, Space previous)
        {
            for (int i = 1; i < 300; i++)
            {
                for (int j = 1; j < 300; j++)
                {
                    previous[i, j].State = present[i, j].State;
                    previous[i, j].Color = present[i, j].Color;
                    previous[i, j].isLocked = present[i, j].isLocked;
                    previous[i, j].isBoundaryGrain = present[i, j].isBoundaryGrain;
                    previous[i, j].isRecrystalized = present[i, j].isRecrystalized;

                }
            }
        }


        public static void msg(String msg)
        {
            Console.WriteLine(msg);
        }


        public static int getIntFromTextBox(TextBox textBox)
        {
            int val = Int32.Parse(textBox.Text);
            return val;
        }

        public static double getDoubleFromTextBox(TextBox textBox)
        {
            double val = Double.Parse(textBox.Text);
            return val;
        }

        public static void exportTxt(String filename, Space s)
        {

            StreamWriter Sw = new StreamWriter(filename + ".txt");


            for (int x = 1; x < 300; x++)
            {
                for (int y = 1; y < 300; y++)
                {
                    Sw.WriteLine(x + "," + y + "," +
                        s[x, y].Color.R + "," +
                        s[x, y].Color.G + "," +
                        s[x, y].Color.B + "," +
                        s[x, y].State);
                }
            }
            Sw.Flush();
            MessageBoxButtons mbb = MessageBoxButtons.OK;
            DialogResult result;
            result = MessageBox.Show("File was saved!", "File save", mbb);
        }

        public static Space removeColorAndSetToEmpty(Space space)
        {
            int numGrainChanged = 0;

            for (int x = 1; x < space.loopSize; x++)
            {
                for (int y = 1; y < space.loopSize; y++)
                {
                    if (space[x, y].Color == Color.Cyan)
                    {
                        space[x, y].State = States.empty;
                        space[x, y].Color = Color.White;
                        space[x, y].isLocked = false;
                        numGrainChanged++;
                    }
                }
            }
            Utils.msg(numGrainChanged + " grains changed");
            return space;
        }
        
        public static Space MarkBoundaryGrains(Space s)
        {
            Color col;
            List<Color> MooreNeighborGrainColors = Factory.getList<Color>();


            for (int x = 1; x < s.loopSize; x++)
            {
                for (int y = 1; y < s.loopSize; y++)
                {

                    if (s[x, y].isBoundaryGrain == false)
                    {

                        col = s[x + 1, y].Color;
                        if (col != Color.White)
                            MooreNeighborGrainColors.Add(col);

                        col = s[x - 1, y + 1].Color;
                        if (col != Color.White)
                            MooreNeighborGrainColors.Add(col);

                        col = s[x - 1, y].Color;
                        if (col != Color.White)
                            MooreNeighborGrainColors.Add(col);

                        col = s[x - 1, y - 1].Color;
                        if (col != Color.White)
                            MooreNeighborGrainColors.Add(col);

                        col = s[x, y - 1].Color;
                        if (col != Color.White)
                            MooreNeighborGrainColors.Add(col);

                        col = s[x + 1, y - 1].Color;
                        if (col != Color.White)
                            MooreNeighborGrainColors.Add(col);

                        col = s[x + 1, y].Color;
                        if (col != Color.White)
                            MooreNeighborGrainColors.Add(col);

                        col = s[x + 1, y + 1].Color;
                        if (col != Color.White)
                            MooreNeighborGrainColors.Add(col);

                        var allAreSame = MooreNeighborGrainColors.All(c => c == MooreNeighborGrainColors.First());
                        MooreNeighborGrainColors.Clear();

                        if (allAreSame == false)
                        {
                            s.BoundaryGrains.Add(s[x, y]);
                            s[x, y].isBoundaryGrain = true;
                        }
                    }

                }
            }

            return s;
        }


        //public static Space ColorBoundaryGrains(Space ev, Space s)
        //{
        //    for (int xx = 1; xx < 301; xx++)
        //    {
        //        for (int yy = 1; yy < 301; yy++)
        //        {
        //            if (s[xx, yy].isBoundaryGrain)
        //            {
        //                ev[xx, yy].Color = Color.Black;
        //            }
        //        }

        //    }
        //    return s;
        //}

        public static Space ColorEnergyView(Space s, int ge, int be)
        {
            int modifiedRGB;

            for (int xx = 1; xx < 301; xx++)
            {
                for (int yy = 1; yy < 301; yy++)
                {

                    modifiedRGB = (int)(265-25 * ge);
                    s[xx, yy].Color = Color.FromArgb(modifiedRGB, modifiedRGB, 255);

                    if (s[xx, yy].isBoundaryGrain)
                    {
                        modifiedRGB = (int)(265-25 * be);
                        s[xx, yy].Color = Color.FromArgb(modifiedRGB, 255, modifiedRGB);

                    }

                    if (s[xx, yy].isRecrystalized)
                    {
                        s[xx, yy].Color = Color.Red;
                    }

                    if ((s[xx, yy].State == States.empty))
                    {

                        s[xx, yy].Color = Color.White;
                    }
                    
                }

            }
            return s;
        }


        public static Space ColorEnergyView(Space s, int ge)
        {
            for (int xx = 1; xx < 301; xx++)
            {
                for (int yy = 1; yy < 301; yy++)
                {
                    if (s[xx, yy].isRecrystalized)
                    {
                        
                        s[xx, yy].Color = Color.Red;
                    }
                    else if((s[xx, yy].State == States.empty))
                    {
                        s[xx, yy].Color = Color.White;
                    }
                    else
                    {
                        int modifiedRGB = (int)(265-25 * ge);
                        s[xx, yy].Color = Color.FromArgb(modifiedRGB, modifiedRGB, 255);
                    }
                }
            }
            return s;
        }


        public static Space assignEnergy(Space s, int GrainEnergy, int BoundaryEnergy)
        {
            for (int x = 1; x < s.loopSize; x++)
            {
                for (int y = 1; y < s.loopSize; y++)
                {
                
                    if (s[x,y].isBoundaryGrain)
                        s[x, y].H = BoundaryEnergy;
                    else
                        s[x, y].H = GrainEnergy;
                }
            }
            return s;
        }

        //public static Space assignGrainEnergy(Space s, int GrainEnergy)
        //{

        //    return s;
        //}

        //public static Space assignBoundaryEnergy(Space s, int BoundaryEnergy)
        //{

        //    return s;
        //}
    }
}
