﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    class RandomNumberGenerator
    {
        private static Random r = Factory.getRandom();

        public static int getRandomInt(int max)
        {
            return r.Next(max);
        }
    }
}
