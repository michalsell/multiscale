﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    class GrainRandomizer
    {
        
         
        public static Space randomizeCA(Space s, int numGrains)
        {
            Utils.msg("Begining CA randomization...");
            Random r = Factory.getRandom();

            for (int i = 0; i < numGrains; i++)
            {
                int x = r.Next(1, 301);
                int y = r.Next(1, 301);

                if (s[x, y].State != States.empty)
                {
                    i--;
                }
                else
                {
                    Color c = RandomColorGenerator.getNoRepeatColor(Space.Colors);
                    s[x, y].Color = c;
                    Space.Colors.Add(c);
                    s[x, y].State = States.grain;
                }
            }

            return s;
        }

        public static Space randomizeMC(Space s, int numColors = 3)
        {
            List<Color> randomColors = Factory.getList<Color>();
            List<Color> excludedColors = Factory.getList<Color>();
            excludedColors.Add(Color.White);
            excludedColors.Add(Color.Black);
            excludedColors.Add(Color.Cyan);

            randomColors = RandomColorGenerator.getNoRepeatListofColors(numColors, excludedColors);

            for (int xx = 1; xx < 301; xx++)
            {
                for (int yy = 1; yy < 301; yy++)
                {
                    if (s[xx, yy].isLocked == false || s[xx, yy].Color == Color.White)
                    {
                        int colorPosition = RandomNumberGenerator.getRandomInt(randomColors.Count);
                        s[xx, yy].Color = randomColors.ElementAt(colorPosition);
                        s[xx, yy].State = States.grain;
                    }
                }

            }
            return s;
        }


        public static Space randomizeRecrystalizedGrains(Space s, int numGrains)
        {
            Random r = Factory.getRandom();

            for (int i = 0; i < numGrains; i++)
            {
                int x = r.Next(1, 301);
                int y = r.Next(1, 301);

                if (s[x, y].isRecrystalized == true)
                {
                    i--;
                }
                else 
                {
                    Color c = RandomColorGenerator.getNoRepeatRedColor(Space.Colors);
                    s[x, y].Color = c;
                    Space.Colors.Add(c);
                    s[x, y].State = States.grain;
                    s[x, y].isRecrystalized = true;
                }
            }

            return s;
        }



        public static Space randomizeRecrystalizedGrainsOnBoundaries(Space s, int numGrains)
        {
            Random r = Factory.getRandom();


            for (int i = 0; i < numGrains; i++)
            {
                int pos = r.Next(0, s.BoundaryGrains.Count);
                Grain grain = s.BoundaryGrains.ElementAt(pos);

                if (grain.isRecrystalized)
                {
                    i--;
                }
                else
                {
                    Color c = RandomColorGenerator.getNoRepeatRedColor(Space.Colors);
                    grain.Color = c;
                    Space.Colors.Add(c);
                    grain.isRecrystalized = true;
                }
            }

            return s;
        }
    }
}
