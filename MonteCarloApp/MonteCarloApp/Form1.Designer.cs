﻿using System.Windows.Forms;

namespace MonteCarloApp
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.rbCA = new System.Windows.Forms.RadioButton();
            this.rbMC = new System.Windows.Forms.RadioButton();
            this.tbGrainCount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbStepsCount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbClickedColor = new System.Windows.Forms.TextBox();
            this.btnRemoveGrains = new System.Windows.Forms.Button();
            this.cbFillEmpty = new System.Windows.Forms.CheckBox();
            this.tbJcoef = new System.Windows.Forms.TextBox();
            this.cbSwitchView = new System.Windows.Forms.CheckBox();
            this.gbEnergyDistr = new System.Windows.Forms.GroupBox();
            this.rbHeterogernous = new System.Windows.Forms.RadioButton();
            this.rbHomogenous = new System.Windows.Forms.RadioButton();
            this.tbBoundaryEnergy = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbGrainEnergy = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.gbSRXGrainOptions = new System.Windows.Forms.GroupBox();
            this.tbNuclConstant = new System.Windows.Forms.TextBox();
            this.gbLocation = new System.Windows.Forms.GroupBox();
            this.rbLocationBoundaries = new System.Windows.Forms.RadioButton();
            this.rbLocationAnywhere = new System.Windows.Forms.RadioButton();
            this.rbNuclBegining = new System.Windows.Forms.RadioButton();
            this.rbNuclIncreasing = new System.Windows.Forms.RadioButton();
            this.rbNuclConstant = new System.Windows.Forms.RadioButton();
            this.btnRecrystalize = new System.Windows.Forms.Button();
            this.gbRecrystalization = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbNumSteps = new System.Windows.Forms.TextBox();
            this.gbGeneration = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbEnergyDistr.SuspendLayout();
            this.gbSRXGrainOptions.SuspendLayout();
            this.gbLocation.SuspendLayout();
            this.gbRecrystalization.SuspendLayout();
            this.gbGeneration.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.pictureBox1.Location = new System.Drawing.Point(9, 10);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(225, 244);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(86, 294);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rbCA
            // 
            this.rbCA.AutoSize = true;
            this.rbCA.Location = new System.Drawing.Point(8, 47);
            this.rbCA.Margin = new System.Windows.Forms.Padding(2);
            this.rbCA.Name = "rbCA";
            this.rbCA.Size = new System.Drawing.Size(39, 17);
            this.rbCA.TabIndex = 2;
            this.rbCA.Text = "CA";
            this.rbCA.UseVisualStyleBackColor = true;
            // 
            // rbMC
            // 
            this.rbMC.AutoSize = true;
            this.rbMC.Checked = true;
            this.rbMC.Location = new System.Drawing.Point(8, 68);
            this.rbMC.Margin = new System.Windows.Forms.Padding(2);
            this.rbMC.Name = "rbMC";
            this.rbMC.Size = new System.Drawing.Size(41, 17);
            this.rbMC.TabIndex = 3;
            this.rbMC.TabStop = true;
            this.rbMC.Text = "MC";
            this.rbMC.UseVisualStyleBackColor = true;
            // 
            // tbGrainCount
            // 
            this.tbGrainCount.Location = new System.Drawing.Point(8, 102);
            this.tbGrainCount.Margin = new System.Windows.Forms.Padding(2);
            this.tbGrainCount.Name = "tbGrainCount";
            this.tbGrainCount.Size = new System.Drawing.Size(50, 20);
            this.tbGrainCount.TabIndex = 4;
            this.tbGrainCount.Text = "2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 87);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Grain count/ Phase count";
            // 
            // tbStepsCount
            // 
            this.tbStepsCount.Location = new System.Drawing.Point(8, 148);
            this.tbStepsCount.Margin = new System.Windows.Forms.Padding(2);
            this.tbStepsCount.Name = "tbStepsCount";
            this.tbStepsCount.Size = new System.Drawing.Size(50, 20);
            this.tbStepsCount.TabIndex = 6;
            this.tbStepsCount.Text = "5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 129);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "MC Steps";
            // 
            // tbClickedColor
            // 
            this.tbClickedColor.BackColor = System.Drawing.Color.White;
            this.tbClickedColor.Location = new System.Drawing.Point(159, 258);
            this.tbClickedColor.Margin = new System.Windows.Forms.Padding(2);
            this.tbClickedColor.Name = "tbClickedColor";
            this.tbClickedColor.Size = new System.Drawing.Size(76, 20);
            this.tbClickedColor.TabIndex = 8;
            // 
            // btnRemoveGrains
            // 
            this.btnRemoveGrains.Enabled = false;
            this.btnRemoveGrains.Location = new System.Drawing.Point(68, 258);
            this.btnRemoveGrains.Margin = new System.Windows.Forms.Padding(2);
            this.btnRemoveGrains.Name = "btnRemoveGrains";
            this.btnRemoveGrains.Size = new System.Drawing.Size(86, 19);
            this.btnRemoveGrains.TabIndex = 9;
            this.btnRemoveGrains.Text = "Remove Grains";
            this.btnRemoveGrains.UseVisualStyleBackColor = true;
            this.btnRemoveGrains.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnRemoveGrains_MouseClick);
            // 
            // cbFillEmpty
            // 
            this.cbFillEmpty.AutoSize = true;
            this.cbFillEmpty.Enabled = false;
            this.cbFillEmpty.Location = new System.Drawing.Point(8, 23);
            this.cbFillEmpty.Margin = new System.Windows.Forms.Padding(2);
            this.cbFillEmpty.Name = "cbFillEmpty";
            this.cbFillEmpty.Size = new System.Drawing.Size(69, 17);
            this.cbFillEmpty.TabIndex = 10;
            this.cbFillEmpty.Text = "Fill empty";
            this.cbFillEmpty.UseVisualStyleBackColor = true;
            // 
            // tbJcoef
            // 
            this.tbJcoef.Location = new System.Drawing.Point(89, 24);
            this.tbJcoef.Name = "tbJcoef";
            this.tbJcoef.Size = new System.Drawing.Size(49, 20);
            this.tbJcoef.TabIndex = 12;
            this.tbJcoef.Text = "1,0";
            // 
            // cbSwitchView
            // 
            this.cbSwitchView.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbSwitchView.AutoSize = true;
            this.cbSwitchView.Enabled = false;
            this.cbSwitchView.Location = new System.Drawing.Point(131, 151);
            this.cbSwitchView.Name = "cbSwitchView";
            this.cbSwitchView.Size = new System.Drawing.Size(76, 23);
            this.cbSwitchView.TabIndex = 13;
            this.cbSwitchView.Text = "Energy View";
            this.cbSwitchView.UseVisualStyleBackColor = true;
            this.cbSwitchView.CheckedChanged += new System.EventHandler(this.cbSwitchView_CheckedChanged);
            // 
            // gbEnergyDistr
            // 
            this.gbEnergyDistr.Controls.Add(this.rbHeterogernous);
            this.gbEnergyDistr.Controls.Add(this.rbHomogenous);
            this.gbEnergyDistr.Controls.Add(this.tbBoundaryEnergy);
            this.gbEnergyDistr.Controls.Add(this.label4);
            this.gbEnergyDistr.Controls.Add(this.cbSwitchView);
            this.gbEnergyDistr.Controls.Add(this.label3);
            this.gbEnergyDistr.Controls.Add(this.tbGrainEnergy);
            this.gbEnergyDistr.Location = new System.Drawing.Point(457, 12);
            this.gbEnergyDistr.Name = "gbEnergyDistr";
            this.gbEnergyDistr.Size = new System.Drawing.Size(213, 180);
            this.gbEnergyDistr.TabIndex = 14;
            this.gbEnergyDistr.TabStop = false;
            this.gbEnergyDistr.Text = "Energy Distribution";
            // 
            // rbHeterogernous
            // 
            this.rbHeterogernous.AutoSize = true;
            this.rbHeterogernous.Location = new System.Drawing.Point(6, 42);
            this.rbHeterogernous.Name = "rbHeterogernous";
            this.rbHeterogernous.Size = new System.Drawing.Size(92, 17);
            this.rbHeterogernous.TabIndex = 1;
            this.rbHeterogernous.TabStop = true;
            this.rbHeterogernous.Text = "Heterogenous";
            this.rbHeterogernous.UseVisualStyleBackColor = true;
            this.rbHeterogernous.CheckedChanged += new System.EventHandler(this.rbHeterogenous_CheckedChanged);
            // 
            // rbHomogenous
            // 
            this.rbHomogenous.AutoSize = true;
            this.rbHomogenous.Checked = true;
            this.rbHomogenous.Location = new System.Drawing.Point(6, 19);
            this.rbHomogenous.Name = "rbHomogenous";
            this.rbHomogenous.Size = new System.Drawing.Size(88, 17);
            this.rbHomogenous.TabIndex = 0;
            this.rbHomogenous.TabStop = true;
            this.rbHomogenous.Text = "Homogenous";
            this.rbHomogenous.UseVisualStyleBackColor = true;
            // 
            // tbBoundaryEnergy
            // 
            this.tbBoundaryEnergy.Enabled = false;
            this.tbBoundaryEnergy.Location = new System.Drawing.Point(9, 138);
            this.tbBoundaryEnergy.Name = "tbBoundaryEnergy";
            this.tbBoundaryEnergy.Size = new System.Drawing.Size(38, 20);
            this.tbBoundaryEnergy.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(118, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Boundary Energy (1-10)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Grain Energy (1-10)";
            // 
            // tbGrainEnergy
            // 
            this.tbGrainEnergy.Location = new System.Drawing.Point(9, 89);
            this.tbGrainEnergy.Name = "tbGrainEnergy";
            this.tbGrainEnergy.Size = new System.Drawing.Size(38, 20);
            this.tbGrainEnergy.TabIndex = 15;
            this.tbGrainEnergy.Text = "3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "J (0,0 - 1,0)";
            // 
            // gbSRXGrainOptions
            // 
            this.gbSRXGrainOptions.BackColor = System.Drawing.SystemColors.Control;
            this.gbSRXGrainOptions.Controls.Add(this.tbNuclConstant);
            this.gbSRXGrainOptions.Controls.Add(this.gbLocation);
            this.gbSRXGrainOptions.Controls.Add(this.rbNuclBegining);
            this.gbSRXGrainOptions.Controls.Add(this.rbNuclIncreasing);
            this.gbSRXGrainOptions.Controls.Add(this.rbNuclConstant);
            this.gbSRXGrainOptions.Location = new System.Drawing.Point(401, 198);
            this.gbSRXGrainOptions.Name = "gbSRXGrainOptions";
            this.gbSRXGrainOptions.Size = new System.Drawing.Size(269, 112);
            this.gbSRXGrainOptions.TabIndex = 20;
            this.gbSRXGrainOptions.TabStop = false;
            this.gbSRXGrainOptions.Text = "Nucleation ";
            // 
            // tbNuclConstant
            // 
            this.tbNuclConstant.Location = new System.Drawing.Point(120, 19);
            this.tbNuclConstant.Name = "tbNuclConstant";
            this.tbNuclConstant.Size = new System.Drawing.Size(34, 20);
            this.tbNuclConstant.TabIndex = 7;
            this.tbNuclConstant.Text = "10";
            // 
            // gbLocation
            // 
            this.gbLocation.Controls.Add(this.rbLocationBoundaries);
            this.gbLocation.Controls.Add(this.rbLocationAnywhere);
            this.gbLocation.Location = new System.Drawing.Point(173, 24);
            this.gbLocation.Name = "gbLocation";
            this.gbLocation.Size = new System.Drawing.Size(90, 64);
            this.gbLocation.TabIndex = 3;
            this.gbLocation.TabStop = false;
            this.gbLocation.Text = "Location";
            // 
            // rbLocationBoundaries
            // 
            this.rbLocationBoundaries.AutoSize = true;
            this.rbLocationBoundaries.Location = new System.Drawing.Point(6, 39);
            this.rbLocationBoundaries.Name = "rbLocationBoundaries";
            this.rbLocationBoundaries.Size = new System.Drawing.Size(78, 17);
            this.rbLocationBoundaries.TabIndex = 1;
            this.rbLocationBoundaries.TabStop = true;
            this.rbLocationBoundaries.Text = "Boundaries";
            this.rbLocationBoundaries.UseVisualStyleBackColor = true;
            // 
            // rbLocationAnywhere
            // 
            this.rbLocationAnywhere.AutoSize = true;
            this.rbLocationAnywhere.Checked = true;
            this.rbLocationAnywhere.Location = new System.Drawing.Point(6, 19);
            this.rbLocationAnywhere.Name = "rbLocationAnywhere";
            this.rbLocationAnywhere.Size = new System.Drawing.Size(72, 17);
            this.rbLocationAnywhere.TabIndex = 0;
            this.rbLocationAnywhere.TabStop = true;
            this.rbLocationAnywhere.Text = "Anywhere";
            this.rbLocationAnywhere.UseVisualStyleBackColor = true;
            // 
            // rbNuclBegining
            // 
            this.rbNuclBegining.AutoSize = true;
            this.rbNuclBegining.Location = new System.Drawing.Point(10, 71);
            this.rbNuclBegining.Name = "rbNuclBegining";
            this.rbNuclBegining.Size = new System.Drawing.Size(79, 17);
            this.rbNuclBegining.TabIndex = 2;
            this.rbNuclBegining.TabStop = true;
            this.rbNuclBegining.Text = "At Begining";
            this.rbNuclBegining.UseVisualStyleBackColor = true;
            // 
            // rbNuclIncreasing
            // 
            this.rbNuclIncreasing.AutoSize = true;
            this.rbNuclIncreasing.Location = new System.Drawing.Point(10, 48);
            this.rbNuclIncreasing.Name = "rbNuclIncreasing";
            this.rbNuclIncreasing.Size = new System.Drawing.Size(74, 17);
            this.rbNuclIncreasing.TabIndex = 1;
            this.rbNuclIncreasing.TabStop = true;
            this.rbNuclIncreasing.Text = "Increasing";
            this.rbNuclIncreasing.UseVisualStyleBackColor = true;
            // 
            // rbNuclConstant
            // 
            this.rbNuclConstant.AutoSize = true;
            this.rbNuclConstant.Checked = true;
            this.rbNuclConstant.Location = new System.Drawing.Point(10, 24);
            this.rbNuclConstant.Name = "rbNuclConstant";
            this.rbNuclConstant.Size = new System.Drawing.Size(67, 17);
            this.rbNuclConstant.TabIndex = 0;
            this.rbNuclConstant.TabStop = true;
            this.rbNuclConstant.Text = "Constant";
            this.rbNuclConstant.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rbNuclConstant.UseVisualStyleBackColor = true;
            // 
            // btnRecrystalize
            // 
            this.btnRecrystalize.Location = new System.Drawing.Point(63, 83);
            this.btnRecrystalize.Name = "btnRecrystalize";
            this.btnRecrystalize.Size = new System.Drawing.Size(75, 23);
            this.btnRecrystalize.TabIndex = 21;
            this.btnRecrystalize.Text = "Start SRX";
            this.btnRecrystalize.UseVisualStyleBackColor = true;
            this.btnRecrystalize.Click += new System.EventHandler(this.btnRecrystalize_Click);
            // 
            // gbRecrystalization
            // 
            this.gbRecrystalization.Controls.Add(this.label6);
            this.gbRecrystalization.Controls.Add(this.tbNumSteps);
            this.gbRecrystalization.Controls.Add(this.tbJcoef);
            this.gbRecrystalization.Controls.Add(this.btnRecrystalize);
            this.gbRecrystalization.Controls.Add(this.label5);
            this.gbRecrystalization.Location = new System.Drawing.Point(251, 198);
            this.gbRecrystalization.Name = "gbRecrystalization";
            this.gbRecrystalization.Size = new System.Drawing.Size(144, 112);
            this.gbRecrystalization.TabIndex = 22;
            this.gbRecrystalization.TabStop = false;
            this.gbRecrystalization.Text = "Recrystalization";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Steps number";
            // 
            // tbNumSteps
            // 
            this.tbNumSteps.Location = new System.Drawing.Point(89, 50);
            this.tbNumSteps.Name = "tbNumSteps";
            this.tbNumSteps.Size = new System.Drawing.Size(49, 20);
            this.tbNumSteps.TabIndex = 22;
            this.tbNumSteps.Text = "10";
            // 
            // gbGeneration
            // 
            this.gbGeneration.Controls.Add(this.cbFillEmpty);
            this.gbGeneration.Controls.Add(this.rbCA);
            this.gbGeneration.Controls.Add(this.rbMC);
            this.gbGeneration.Controls.Add(this.label1);
            this.gbGeneration.Controls.Add(this.tbGrainCount);
            this.gbGeneration.Controls.Add(this.label2);
            this.gbGeneration.Controls.Add(this.tbStepsCount);
            this.gbGeneration.Location = new System.Drawing.Point(251, 12);
            this.gbGeneration.Name = "gbGeneration";
            this.gbGeneration.Size = new System.Drawing.Size(200, 180);
            this.gbGeneration.TabIndex = 23;
            this.gbGeneration.TabStop = false;
            this.gbGeneration.Text = "Structure Generation";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(682, 335);
            this.Controls.Add(this.gbGeneration);
            this.Controls.Add(this.gbRecrystalization);
            this.Controls.Add(this.gbSRXGrainOptions);
            this.Controls.Add(this.gbEnergyDistr);
            this.Controls.Add(this.btnRemoveGrains);
            this.Controls.Add(this.tbClickedColor);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Structure";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbEnergyDistr.ResumeLayout(false);
            this.gbEnergyDistr.PerformLayout();
            this.gbSRXGrainOptions.ResumeLayout(false);
            this.gbSRXGrainOptions.PerformLayout();
            this.gbLocation.ResumeLayout(false);
            this.gbLocation.PerformLayout();
            this.gbRecrystalization.ResumeLayout(false);
            this.gbRecrystalization.PerformLayout();
            this.gbGeneration.ResumeLayout(false);
            this.gbGeneration.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public PictureBox pictureBox1;
        private Button button1;
        private RadioButton rbCA;
        private RadioButton rbMC;
        private TextBox tbGrainCount;
        private Label label1;
        private TextBox tbStepsCount;
        private Label label2;
        private TextBox tbClickedColor;
        private Button btnRemoveGrains;
        private CheckBox cbFillEmpty;
        private TextBox tbJcoef;
        private CheckBox cbSwitchView;
        private GroupBox gbEnergyDistr;
        private RadioButton rbHeterogernous;
        private RadioButton rbHomogenous;
        private TextBox tbGrainEnergy;
        private TextBox tbBoundaryEnergy;
        private Label label3;
        private Label label4;
        private Label label5;
        private GroupBox gbSRXGrainOptions;
        private RadioButton rbNuclBegining;
        private RadioButton rbNuclIncreasing;
        private RadioButton rbNuclConstant;
        private Button btnRecrystalize;
        private GroupBox gbLocation;
        private RadioButton rbLocationBoundaries;
        private RadioButton rbLocationAnywhere;
        private GroupBox gbRecrystalization;
        private Label label6;
        private TextBox tbNumSteps;
        private TextBox tbNuclConstant;
        private GroupBox gbGeneration;
    }
}

