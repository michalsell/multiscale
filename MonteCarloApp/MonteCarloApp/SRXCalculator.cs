﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    class SRXCalculator
    {
        public static Space calculate(Space s, int stepsNum, int numGrains, double J, NucleationType nucleationType, Locations locations)
        {
            int debugCounter = 0;
            
            List<Grain> GrainsFromSpace = Factory.getList<Grain>();
            List<Grain> MooreNeighborGrains = Factory.getList<Grain>();
            List<Color> MooreNeighborGrainColors = Factory.getList<Color>();

            if(nucleationType == NucleationType.BEGINING)
                GrainRandomizer.randomizeRecrystalizedGrains(s, numGrains);


            for (int i = 0; i < stepsNum; i++)
            {
                if(nucleationType == NucleationType.CONSTANT)
                    if(locations == Locations.ANYWHERE)
                        GrainRandomizer.randomizeRecrystalizedGrains(s, numGrains);
                    else
                        GrainRandomizer.randomizeRecrystalizedGrainsOnBoundaries(s, numGrains);

                if (nucleationType == NucleationType.INCREASING)
                    if (locations == Locations.ANYWHERE)
                        GrainRandomizer.randomizeRecrystalizedGrains(s, i * numGrains);
                    else
                        GrainRandomizer.randomizeRecrystalizedGrainsOnBoundaries(s, i * numGrains);

                //copy grains from Space to list 
                for (int x = 1; x < s.loopSize; x++)
                {
                    for (int y = 1; y < s.loopSize; y++)
                    {
                        if (s[x, y].State == States.grain)
                        {
                            GrainsFromSpace.Add(s[x, y]);
                        }
                    }
                }

                //shuffle grains
                GrainsFromSpace.Shuffle();


                foreach (var Grain in GrainsFromSpace)
                {


                    {
                        MooreNeighborGrains.Add(s[Grain.x + 1, Grain.y]);

                        MooreNeighborGrains.Add(s[Grain.x - 1, Grain.y + 1]);

                        MooreNeighborGrains.Add(s[Grain.x - 1, Grain.y]);

                        MooreNeighborGrains.Add(s[Grain.x - 1, Grain.y - 1]);

                        MooreNeighborGrains.Add(s[Grain.x, Grain.y - 1]);

                        MooreNeighborGrains.Add(s[Grain.x + 1, Grain.y - 1]);

                        MooreNeighborGrains.Add(s[Grain.x + 1, Grain.y]);

                        MooreNeighborGrains.Add(s[Grain.x + 1, Grain.y + 1]);
                    }



                    var recrystalized = MooreNeighborGrains.Where(r => r.isRecrystalized == true && r.State != States.empty).ToList();

                    if (recrystalized.Count > 0)
                        debugCounter++;

                    //if site has recrystalized neighbor
                    if (recrystalized.Count() > 0)
                    {
                        foreach (var g in MooreNeighborGrains)
                        {
                            MooreNeighborGrainColors.Add(g.Color);
                        }

                        Predicate < Color > White = IsWhite;
                        MooreNeighborGrainColors.RemoveAll(White);


                        Color RecrystalizedColor = recrystalized.ElementAt(RandomNumberGenerator.getRandomInt(recrystalized.Count())).Color;

                        //calculate energy before reorientation
                        double energyBefore = J * EnergyCalc.calculateMooreEnergy(Grain.Color, MooreNeighborGrainColors) + Grain.H;
                        //calculate energy after reorientation
                        double energyAfter = J * EnergyCalc.calculateMooreEnergy(RecrystalizedColor, MooreNeighborGrainColors);
                        //compare if the energy after is lower -> accept change
                        if (energyAfter < energyBefore)
                        {
                            Grain.Color = RecrystalizedColor;
                            Grain.isRecrystalized = true;
                        }

                    }

                    MooreNeighborGrains.Clear();
                    MooreNeighborGrainColors.Clear();
                    recrystalized.Clear();

                }

                GrainsFromSpace.Clear();
            }            
            Utils.MarkBoundaryGrains(s);
            Utils.msg("times site had recr. neighbor: " + debugCounter);
            return s;
        }

        static bool IsWhite(Color c)
        {
            if (c == Color.White)
                return true;
            else
                return false;
        }

    }
}
