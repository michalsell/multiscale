﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonteCarloApp
{
    class Drawer
    {
        PictureBox pb;

        public Drawer(PictureBox pb)
        {
            this.pb = pb;
        }

        public void drawSpace(Space s)
        {
            Bitmap DrawArea;
            DrawArea = Factory.getBitmap(300, 300);

            Graphics g;
            g = Graphics.FromImage(DrawArea);

            for (int i = 1; i < 300; i++)
            {
                for (int j = 1; j < 300; j++)
                {
                    Pen GrainPen = Factory.getPen(s[i, j].Color);

                    g.DrawRectangle(GrainPen, i - 1, j - 1, 1, 1);

                }
            }

            pb.Image = DrawArea;
        }
    }
}
