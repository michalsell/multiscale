﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MonteCarloApp
{
    class Factory
    {
        public static Grain getGrain()
        {
            return new Grain();
        }

        public static Grain[,] getGrainArray(int size)
        {
            return new Grain[size, size];
        }

        public static Space getSpace()
        {
            return new Space();
        }

        public static Calculator getCalculator()
        {
            return new Calculator();
        }

        public static List<T> getList<T>()
        {
            return new List<T>();
        }

        public static Drawer getDrawer(PictureBox pb)
        {
            return new Drawer(pb);
        }

        public static Bitmap getBitmap(int x, int y)
        {
            return new Bitmap(x, y);
        }

        public static Pen getPen(Color c)
        {
            return new Pen(c);
        }


        public static Random getRandom()
        {
            return new Random(); 
        }
    }
}
