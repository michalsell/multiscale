﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    internal class Grain
    {
        public Color Color
        {
            get { return color; }
            set
            {
                if(value == Color.White && State == States.grain) { throw Exception(); }
                color = value;
            }
        }


        public bool isLocked;
        private bool IsRecrystalized = false;

        public bool isRecrystalized
        {
            get { return IsRecrystalized; }
            set
            {
                IsRecrystalized = value;
                if(value == true)
                    H = 0;
            }
        }
        public bool isBoundaryGrain { get; set; }
        private int Hh;

        public int H {
            get { return Hh; }
            set
            {
                Hh = value;
                //if(isRecrystalized)
                //    Hh = 0;
            }
        }
        public States State { get; set; }
        public int x { get; set; }
        public int y {get; set; }

        public Grain()
        {
            Color = Color.White;
            State = States.empty;
            isLocked = false;
        }
        
        private Color color;
        private Exception Exception()
        {
            throw new Exception("White color was set to active grain.");
        }
    }
}
