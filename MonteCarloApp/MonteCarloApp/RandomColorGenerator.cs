﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    class RandomColorGenerator
    {
        public static Color getRandomColor()
        {
            Random r = new Random();
            Color c = Color.FromArgb(0, r.Next(0, 256), r.Next(0, 256));
            return c;
        }

        public static Color getRandomRedColor()
        {
            Random r = new Random();
            //Color c = Color.FromArgb(255, r.Next(0, 256), r.Next(0, 256));
            Color c = Color.FromArgb(r.Next(0, 256), 0, 0);
            return c;
        }

        public static Color getNoRepeatColor(List<Color> excludedColors)
        {
            Color c;

                do
                {
                    c = getRandomColor();

                } while (excludedColors.Contains(c));

            return c;
        }


        public static Color getNoRepeatRedColor(List<Color> excludedColors)
        {
            Color c;

            do
            {
                c = getRandomRedColor();

            } while (excludedColors.Contains(c));

            return c;
        }


        public static List<Color> getNoRepeatListofColors(int numColors)
        {
            Color c;
            List<Color> Colors = Factory.getList<Color>();

            for (int i = 0; i < numColors; i++)
            {
                do
                {
                    c = getRandomColor();

                } while (Colors.Contains(c));
                Colors.Add(c);
            }

            return Colors ;
        }

        public static List<Color> getNoRepeatListofColors(int numColors, List<Color> excludedColors)
        {
            Color c;
            List<Color> Colors = Factory.getList<Color>();

            for (int i = 0; i < numColors; i++)
            {
                c = getNoRepeatColor(excludedColors);
                Colors.Add(c);
                excludedColors.Add(c);
                
            }
            return Colors;
        }
    }
}
