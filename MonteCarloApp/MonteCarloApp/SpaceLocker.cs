﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    class SpaceLocker
    {
        public static void LockSpace(Space s)
        {
            for (int x = 0; x < s.loopSize; x++)
            {
                for (int y = 0; y < s.loopSize; y++)
                {
                    s[x, y].isLocked = true;
                }
            }
        }
    }
}
