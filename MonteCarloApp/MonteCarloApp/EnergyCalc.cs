﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloApp
{
    class EnergyCalc
    {
        public static int calculateMooreEnergy(Color colorInTheMiddle , List<Color> colors)
        {
            //input colors on this list may repeat!      
            int energy = 0;

            foreach (var color in colors)
            {
                if(color != colorInTheMiddle)
                {
                    energy++;
                }

            }           
            return energy;
        }


        public static Color getRandomColorMinimizingEnergy(Color colorInTheMiddle, List<Color> colors)
        {
            Color minimizingColor = colorInTheMiddle;
            int newEnergy;
            //calculate energy for the first time
            int inputEnergy = calculateMooreEnergy(minimizingColor, colors);
            

            do
            {
                //change color for the random color that appears in the neighborhood
                int rand = RandomNumberGenerator.getRandomInt(colors.Count);
                if (colors.Count > 0)
                {
                    minimizingColor = colors.ElementAt(rand);
                    //calculate energy again
                    newEnergy = calculateMooreEnergy(minimizingColor, colors);
                    //if the new energy is lower -> accept the change
                }
                else
                    break;
            } while (inputEnergy < newEnergy);

            return minimizingColor;
        }
    }
}
